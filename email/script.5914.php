<?php

// если скрипт запущен не из под консоли, запрещаем доступ
$sapi_type = php_sapi_name();

if (substr($sapi_type, 0, 3) != 'cli') {
    echo 'access denied';
    die();
}

$_SERVER['DOCUMENT_ROOT'] = realpath(__DIR__ . '/../..');
$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];

define('NO_KEEP_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS', true);
define('BX_CRONTAB', true);
define('BX_NO_ACCELERATOR_RESET', true);

require_once($DOCUMENT_ROOT . '/bitrix/modules/main/include/prolog_before.php');

$error = array();
if (\Bitrix\Main\Loader::includeModule('iblock')) {

    $properties = CIBlockProperty::GetList(Array(), Array("CODE" => "CARD_REGISTER", "IBLOCK_ID" => \Tools\Constants::$IBLOCK_APPLICATIONS));
    if (!$prop_fields = $properties->GetNext()) {
        $arFields = Array(
            "NAME" => "Отправить на дозаполнением клиентом",
            "ACTIVE" => "Y",
            "SORT" => "500",
            "CODE" => "CARD_REGISTER",
            "PROPERTY_TYPE" => "S",
            "USER_TYPE" => "CardRegister",
            "IBLOCK_ID" => \Tools\Constants::$IBLOCK_APPLICATIONS
        );
        $ibp = new CIBlockProperty;
        if ($PropID = $ibp->Add($arFields)) {
            $errors[] = 'Свойство добавлено CARD_REGISTER';
        } else {
            $errors[] = 'Ошибка добавления свойства CARD_REGISTER: ' . $ibp->LAST_ERROR;
        }
    } else {
        $errors[] = 'Свойство уже есть CARD_REGISTER';
    }



    $rsET = CEventType::GetListEx(array(), array("EVENT_NAME" => 'REGISTER_UPDATE'));
    if ($arET = $rsET->Fetch()) {
        $errors[] = 'Тип события уже есть REGISTER_UPDATE';
    } else {
        $et = new CEventType;
        $et->Add(array(
            "LID" => SITE_ID,
            "EVENT_NAME" => 'REGISTER_UPDATE',
            "NAME" => 'REGISTER_UPDATE',
            "DESCRIPTION" => 'REGISTER_UPDATE'
        ));
        $errors[] = 'Cобытие добавлено REGISTER_UPDATE';
    }
    $arFilter = Array(
        "TYPE" => "REGISTER_UPDATE",
    );
    $rsMess = CEventMessage::GetList($by = "site_id", $order = "desc", $arFilter);
    if ($arET = $rsMess->Fetch()) {
        $errors[] = 'Сообщение уже есть REGISTER_UPDATE';
    } else {
        $et = new CEventMessage;
        $et->Add(array(
            "ACTIVE" => 'Y',
            "EVENT_NAME" => 'REGISTER_UPDATE',
            "LID" => array(SITE_ID),
            "EMAIL_FROM" => '#DEFAULT_EMAIL_FROM#',
            "EMAIL_TO" => '#EMAIL#',
            "SUBJECT" => 'Обновите данные для регистрации карты',
            "BODY_TYPE" => 'html',
            "MESSAGE" => '#CLIENT#, обновите данные для регистрации карты по <a href="#LINK#">ссылке</a>',
        ));
        $errors[] = 'Сообщение добавлено REGISTER_UPDATE';
    }
}
echo implode(PHP_EOL, $errors) . PHP_EOL;
