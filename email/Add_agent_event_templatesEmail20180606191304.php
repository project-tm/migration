<?php

namespace Sprint\Migration;

use CAgent,
    CEventType,
    CEventMessage,
    Bitrix\Main\EventManager;

class Add_agent_event_templatesEmail20180606191304 extends \Adv\Bitrixtools\Migration\SprintMigrationBase
{

    const AGENT = 'Adv\Agent\Absence::run();';
    const EVENT_TYPE = [
        'ADV_ABSENCE_DOUBLE_DAY' => [
            "NAME"             => 'Дублирование отсутствия из 1с',
            "DESCRIPTION"      => '#ID# - ID записи
#NAME# - Название отсутствия
#ACTIVE_FROM# - Начало
#ACTIVE_TO# - Окончание
#ABSENCE_TYPE# - Тип
#MESSAGE# - Сообщение',
            'EMAIL_FROM'       => '#DEFAULT_EMAIL_FROM# ',
            'EMAIL_TO'         => '#DEFAULT_EMAIL_FROM#',
            'SUBJECT'          => 'Дублируется отсутствие из 1с',
            'MESSAGE'          => '
<p style="text-align: left;">
<a href="//#SERVER_NAME#/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=3&type=structure&ID=#ID#&lang=ru&find_section_section=0&WF=Y">Дублируется отсутствие из 1с</a><br>
</p>
<p style="text-align: left;">
#ID# - ID записи<br>
#NAME# - Название отсутствия<br>
#ACTIVE_FROM# - Начало<br>
#ACTIVE_TO# - Окончание<br>
#ABSENCE_TYPE# - Тип<br>
#MESSAGE# - Сообщение
</p>',
            'BODY_TYPE'        => 'html',
            'SITE_TEMPLATE_ID' => 'mail_user',
        ],
        'ADV_ABSENCE_EMPTY_1C'   => [
            "NAME"             => 'Записи об тсутствии нет в 1с',
            "DESCRIPTION"      => '#MESSAGE# - Сообщение',
            'EMAIL_FROM'       => '#DEFAULT_EMAIL_FROM# ',
            'EMAIL_TO'         => '#DEFAULT_EMAIL_FROM#',
            'SUBJECT'          => 'Записи об тсутствии нет в 1с',
            'MESSAGE'          => '
<p style="text-align: left;">
#MESSAGE# - Сообщение
</p>',
            'BODY_TYPE'        => 'html',
            'SITE_TEMPLATE_ID' => 'mail_user',
        ],
    ];

    protected $description = "Интеграция 1С и Б24 по отсуствиям. Создание шаблонов для письмем, агента, событий";

    public function up()
    {
        $helper = new HelperManager();


        /*
         * агенты
         */
        $name = self::AGENT;
        $module = "";
        $period = "Y";
        $interval = 60 * 60 * 24;
        $res = CAgent::GetList([], ["NAME" => $name]);
        if (!$res->Fetch()) {
            CAgent::AddAgent(
                $name, $module, $period, $interval, $datecheck, $active
            );
        }

        foreach (self::EVENT_TYPE as $eventName => $eventData) {
            $arFilter = [
                "TYPE_ID" => $eventName,
                "LID"     => "ru",
            ];
            $res = CEventType::GetList($arFilter);
            if (!$res->Fetch()) {
                {
                    $et = new CEventType;
                    $et->Add([
                        'SORT'        => '150',
                        "LID"         => 'ru',
                        "EVENT_NAME"  => $eventName,
                        "NAME"        => $eventData['NAME'],
                        "DESCRIPTION" => $eventData['DESCRIPTION'],
                    ]);
                }
            }

            $arFilter = [
                "TYPE_ID" => $eventName,
            ];
            $res = CEventMessage::GetList($by = "site_id", $order = "desc", $arFilter);
            if (!$res->Fetch()) {
                $emess = new CEventMessage;
                $emess->Add([
                    'ACTIVE'           => 'Y',
                    'EVENT_NAME'       => $eventName,
                    'LID'              =>
                        [
                            0 => 's1',
                        ],
                    'EMAIL_FROM'       => $eventData['EMAIL_FROM'],
                    'EMAIL_TO'         => $eventData['EMAIL_TO'],
                    'BCC'              => '',
                    'CC'               => '',
                    'REPLY_TO'         => '',
                    'IN_REPLY_TO'      => '',
                    'PRIORITY'         => '',
                    'FIELD1_NAME'      => null,
                    'FIELD1_VALUE'     => null,
                    'FIELD2_NAME'      => null,
                    'FIELD2_VALUE'     => null,
                    'SUBJECT'          => $eventData['SUBJECT'],
                    'MESSAGE'          => $eventData['MESSAGE'],
                    'BODY_TYPE'        => $eventData['BODY_TYPE'],
                    'SITE_TEMPLATE_ID' => $eventData['SITE_TEMPLATE_ID'],
                    'ADDITIONAL_FIELD' =>
                        [
                        ],
                    'LANGUAGE_ID'      => '',
                ]);
            }
        }

        /*
         * события
         */
        EventManager::getInstance()->registerEventHandler('iblock', 'OnBeforeIBlockElementAdd', 'iblock',
            'Adv\Handlers\Absence', 'OnBeforeIBlockElementAdd');
        EventManager::getInstance()->registerEventHandler('iblock', 'OnAfterIBlockElementAdd', 'iblock',
            'Adv\Handlers\Absence', 'OnAfterIBlockElementAdd');
//
//        EventManager::getInstance()->registerEventHandler('iblock', 'OnAfterIBlockElementAdd', 'iblock',
//            'Adv\Handlers\StateHistory', 'OnAfterIBlockElementAdd');
//        EventManager::getInstance()->registerEventHandler('iblock', 'OnAfterIBlockElementUpdate', 'iblock',
//            'Adv\Handlers\StateHistory', 'OnAfterIBlockElementAdd');
    }

    public function down()
    {
        $helper = new HelperManager();

        CAgent::RemoveAgent(self::AGENT);

        $emessage = new CEventMessage;
        foreach (self::EVENT_TYPE as $eventName => $eventData) {
            $arFilter = [
                "TYPE_ID" => $eventName,
            ];
            $res = CEventMessage::GetList($by = "site_id", $order = "desc", $arFilter);
            while ($arItem = $res->Fetch()) {
                $emessage->Delete($arItem['ID']);
            }
            $et = new CEventType;
            $et->Delete($eventName);
        }

        EventManager::getInstance()->unRegisterEventHandler('iblock', 'OnBeforeIBlockElementAdd', 'iblock',
            'Adv\Handlers\Absence', 'OnBeforeIBlockElementAdd');
        EventManager::getInstance()->unRegisterEventHandler('iblock', 'OnAfterIBlockElementAdd', 'iblock',
            'Adv\Handlers\Absence', 'OnAfterIBlockElementAdd');
//
//        EventManager::getInstance()->unRegisterEventHandler('iblock', 'OnAfterIBlockElementAdd', 'iblock',
//            'Adv\Handlers\StateHistory', 'OnAfterIBlockElementAdd');
//        EventManager::getInstance()->unRegisterEventHandler('iblock', 'OnAfterIBlockElementUpdate', 'iblock',
//            'Adv\Handlers\StateHistory', 'OnAfterIBlockElementAdd');
    }

}
