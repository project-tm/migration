<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

use Bitrix\Main\Loader,
    Bitrix\Main\Application;

Loader::includeModule('crm');
Loader::includeModule('tm.portal');


$request = Application::getInstance()->getContext()->getRequest();
$page = $request->get('page') ?: 1;

$connection = \Bitrix\Main\Application::getConnection();

$USEE_ID = 548;
$DATE_START = '30.10.2017 11:40:00';
$DATE_END = '30.10.2017 12:35:00';
$TIME_DATE_START = Tm\Portal\Utility\Date::toTime($DATE_START);
$TIME_DATE_END = Tm\Portal\Utility\Date::toTime($DATE_END);

pre($TIME_DATE_START, $TIME_DATE_END);
//2017-10-30 13:52:12
//2017-10-30 11:40:00
//2017-10-30 12:40:00
//mysql test_test < portalb24-2017-10-30-16-27-13.sql
//SELECT `ID`, `DATE_MODIFY` FROM `b_crm_lead` WHERE ID IN(SELECT `EVENT_ID` FROM b_crm_event)
//exit;

$oLead = new CCrmLead;
searchLeadDate(array(
    '!CREATED_BY_ID' => array($USEE_ID, 624),
    '<CREATED_BY_ID' => $page * 10,
    '=ENTITY_TYPE' => 'LEAD',
    '=ENTITY_FIELD' => 'STATUS_ID',
//    'ID' => 1888770,
//    'ENTITY_ID' => 33687,
//    '>=DATE_CREATE' => $DATE_START,
//    '<=DATE_CREATE' => $DATE_END,
));
//exit;
if ($page == 1) {
    searchLeadDate(array(
        '=CREATED_BY_ID' => $USEE_ID,
        '=ENTITY_TYPE' => 'LEAD',
//        'ENTITY_ID' => 34078,
        '=ENTITY_FIELD' => 'STATUS_ID',
        array(
            'LOGIC' => 'OR',
            '<=DATE_CREATE' => $DATE_START,
            '>=DATE_CREATE' => $DATE_END,
        )
    ));
//    exit;
}

if ($page > 80) {
    echo '<h4>' . (time() - $time) . 'сек </h4>';
} else {
    $param = array(
        'page' => ++$page,
        'time' => time(),
    );
    pre($page);
    echo '<h4>' . (time() - $time) . 'сек </h4>';
    echo '<META http-equiv="refresh" content="1; URL=?' . http_build_query($param) . '">';
    echo '<a href="?' . http_build_query($param) . '">next</>';
}

function searchLeadDate($arFilter) {
    pre($arFilter);
    $isSearch = false;
    global $USER_FIELD_MANAGER;
    $rs = CCrmEvent::GetList(array(), $arFilter);
    $arLead = array();
    while ($arEvent = $rs->Fetch()) {
//        preExit($arEvent);
        $arLead[$arEvent['ENTITY_ID']] = $arEvent['DATE_CREATE'];
//        pre(CCrmLead::getById($arEvent['ENTITY_ID'])['ID']);
        $arPostFields = $USER_FIELD_MANAGER->GetUserFields("CRM_LEAD", $arEvent['ENTITY_ID'], LANGUAGE_ID);
        if ($arPostFields['UF_CRM_1517819261']['VALUE'] == $arEvent['DATE_CREATE']) {
            continue;
        }
//        pre($arPostFields['UF_CRM_1517819261']['VALUE']);
        $USER_FIELD_MANAGER->Update('CRM_LEAD', $arEvent['ENTITY_ID'], array(
            'UF_CRM_1517819261' => $arEvent['DATE_CREATE']
        ));
    }
}
