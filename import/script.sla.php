<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

//\Bitrix\Main\Loader::includeModule('tm.portal');
//
//$connection = \Bitrix\Main\Application::getConnection();

use Project\Tools;

$filename = __DIR__ . '/data/sla.csv';
if (($handle = fopen($filename, "r")) !== FALSE) {
//                pre($handle);
    $key = 0;
    while (($data = fgetcsv($handle, 10000, ";")) !== FALSE) {

        $key++;
        if ($key > 1) {
            $data = array_map('trim', $data);
//            pre($data);
            $iblockId = Tools\Config::taskSlaIblocId();
            $arFields = array(
                'DATE_ACTIVE_FROM' => ConvertTimeStamp(time(), 'FULL'),
                'TIMESTAMP_X' => ConvertTimeStamp(time(), 'FULL'),
                'DATE_CREATE' => ConvertTimeStamp(time(), 'FULL'),
                'IBLOCK_ID' => $iblockId,
                'IBLOCK_SECTION_ID' => $data[6] ? Tools\Update\Section::getByNameTree($iblockId, $data[6]) : 0,
                'NAME' => $data[1],
                'SORT' => $data[2],
                'ACTIVE' => 'Y',
                'DETAIL_TEXT' => '',
                'DETAIL_TEXT_TYPE' => 'html',
                'PREVIEW_TEXT' => '',
                'PREVIEW_TEXT_TYPE' => 'html',
            );
            $propFields = array(
                'TYPE' => $data[5] ? Tools\Update\Property::props($iblockId, 607, $data[5]) : '',
                'TIME' => $data[3],
                'PODSKAZKA' => $data[4],
            );
            if (empty($arFields['NAME'])) {
                continue;
            }

            $arFiter = array(
                'IBLOCK_ID' => $arFields['IBLOCK_ID'],
                'SECTION_ID' => $arFields['IBLOCK_SECTION_ID'],
                'NAME' => $arFields['NAME'],
            );
            $arItem = Tools\Update\Iblock::searchByFilter($arFiter, $arFields, $propFields);


//            preExit($arItem, $arFields, $propFields);
        }
//        if ($key < $start) {
//            continue;
//        }
//        if ($key >= $end) {
//            return true;
//        }
//        if (empty($key) or empty($data)) {
//            continue;
//        }
//        $arData = array_map(function($value) {
//            return iconv('CP1251', "UTF-8", $value);
//        }, $data);
//        static::importProduct($arData);
////                    exit;
    };
    fclose($handle);
}