<?

use Bitrix\Main\Loader,
    Bitrix\Main\Application;

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

if (Loader::includeModule('import.catalog')) {
    $connect = Application::getConnection('old');

    $request = Application::getInstance()->getContext()->getRequest();
    $page = $request->get('page') ?: 1;

    $limit = 100;
    $start = ($page - 1) * $limit;
    $end = ($page) * $limit;

    $arData = file(__DIR__ . '/catalog.search.404.txt');
    $isSearch = false;
    foreach ($arData as $key => $url) {
        if ($key < $start) {
            continue;
        }
        if ($key >= $end) {
            return true;
        }
        $isSearch = true;

//        pre($key, $value);
        $value = explode('/', $url);
        $code = $value[6];

        $arSelect = Array(
            'ID',
        );
        $arFilter = Array(
            "IBLOCK_ID" => 2,
            'CODE' => $code
        );
//        pre($arFilter);
        $arItem = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect)->GetNext();
        if (!$arItem) {
            $record = $connect->query('SELECT id FROM items WHERE alias="' . $DB->ForSql($code) . '"')
                    ->fetch();
            $arFilter = Array(
                "IBLOCK_ID" => 2,
//                'ACTIVE' => 'Y',
                'PROPERTY_OLD_ID' => $record['id']
            );
            $arItem = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect)->GetNext();

            if (!$arItem) {
                echo '<br><a href="' . $url . '" target="_blank">' . $url . '</a>';
            } else {
//                pre($arItem);
            }
//            exit;
        } else {
//            pre($arItem);
        }
    };
}
