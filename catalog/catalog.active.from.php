<?

use Bitrix\Main\Loader,
    Bitrix\Main\Application;

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

if (Loader::includeModule('import.catalog')) {
    $request = Application::getInstance()->getContext()->getRequest();
    $page = $request->get('page') ?: 1;

    $time = time();
    $limit = 30;
    $IBLOCK_ID = 2;
    $arFilter = Array(
        "IBLOCK_ID" => $IBLOCK_ID,
//        "ACTIVE_FROM" => false,
    );
    $arSelect = array(
        "ID",
        'ACTIVE_FROM'
    );
    


    $res = CIBlockElement::GetList(array('ACTIVE_FROM'=>'nulls,asc'), $arFilter, false, Array("nPageSize" => $limit, "iNumPage" => $page), $arSelect);
    $arResult['PAGE_COUNT'] = $res->NavPageCount;
    $arResult['PAGE_ITEM'] = $res->NavPageNomer;
    $arResult['PAGE_IS_NEXT'] = $arResult['PAGE_ITEM'] < $arResult['PAGE_COUNT'];

    $count = $limit * $arResult['PAGE_COUNT'];
    echo '<h3>Выполнено ' . round(($page - 1) / ceil($count / $limit) * 100, 5) . '% (' . ($page - 1) * $limit . '/' . $res->SelectedRowsCount() . ')</h3>';
    $el = new CIBlockElement;
    while ($arItem = $res->Fetch()) {
        preExit($arItem);
        if (empty($arItem['ACTIVE_FROM'])) {
            $arFields = array();
            $arFields['ACTIVE_FROM'] = '01.05.2017 00:00:00';
            pre($arFields);
            $el->Update($arItem['ID'], $arFields);
        }
    }
    exit;
    echo '<h4>' . (time() - $time) . 'сек </h4>';
    if ($arResult['PAGE_IS_NEXT']) {
        $param = array(
            'page' => ++$page,
            'time' => time(),
        );
        echo '<META http-equiv="refresh" content="1; URL=?' . http_build_query($param) . '">';
        echo '<a href="?' . http_build_query($param) . '">next</>';
    } else {
        //    echo '<META http-equiv="refresh" content="1; URL=/import/product.clear.php">';
    }
}
