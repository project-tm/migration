<?

use Bitrix\Main\Loader,
    Bitrix\Main\Application;

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

if (Loader::includeModule('import.catalog')) {
    $request = Application::getInstance()->getContext()->getRequest();
    $page = $request->get('page') ?: 1;

    $time = time();
    $limit = 100;
    $start = ($page - 1) * $limit;
    $end = ($page) * $limit;

    $el = new CIBlockElement;
    $fileDectivate = $_SERVER["DOCUMENT_ROOT"] . '/upload/tmp/log/dectivate.txt';
    $products = array();
    $key = -1;
    foreach (file($fileDectivate) as $list) {
        $key++;
        if ($key < $start) {
            continue;
        }
        if ($key >= $end) {
            continue;
        }
        foreach (array_map('ceil', explode(',', $list)) as $ID) {
//            $arFields = array();
//            $arFields['ACTIVE'] = 'N';
//            pre($ID, $arFields);
////            pre($ID);
            $products[] = $ID;
//            $el->Update($ID, $arFields);
        }
    }
//    preExit($products);

    if ($products) {
        $CATALOG_ID = 2;
        $arFilter = Array(
            "IBLOCK_ID" => $CATALOG_ID,
            "ACTIVE" => 'N',
            "ID" => $products,
        );
        pre($arFilter);
        $arSelect = array(
            'ID'
        );
        $res = CIBlockElement::GetList(array(), $arFilter, false, Array("nPageSize" => $limit, "iNumPage" => 1), $arSelect);
        $arResult['PAGE_COUNT'] = $res->NavPageCount;
        $arResult['PAGE_ITEM'] = $res->NavPageNomer;
        $arResult['PAGE_IS_NEXT'] = $arResult['PAGE_ITEM'] < $arResult['PAGE_COUNT'];

        $count = $limit * $arResult['PAGE_COUNT'];
        echo '<h3>Выполнено ' . round(($page - 1) / ceil($count / $limit) * 100, 5) . '% (' . ($page - 1) * $limit . '/' . $res->SelectedRowsCount() . ')</h3>';
        while ($arItem = $res->Fetch()) {
            $arFields = array();
            $arFields['ACTIVE'] = 'Y';
            pre($arItem["ID"]);
            $el->Update($arItem["ID"], $arFields);
//            exit;
        }
        $param = array(
            'page' => ++$page,
            'time' => time(),
        );
        echo '<h4>' . (time() - $time) . 'сек </h4>';
        echo '<META http-equiv="refresh" content="1; URL=?' . http_build_query($param) . '">';
        echo '<a href="?' . http_build_query($param) . '">next</>';
    }
    echo '<h4>' . (time() - $time) . 'сек </h4>';

}
