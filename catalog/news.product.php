<?

use Bitrix\Main\Loader,
    Bitrix\Main\Application;

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

if (Loader::includeModule('project.core')) {
    $request = Application::getInstance()->getContext()->getRequest();
    if(empty($page)) {
        $_SESSION['news'] = array();
    }
    $page = $request->get('page') ?: 1;

    $time = time();
    $limit = 30;
    $arFilter = Array(
        "IBLOCK_ID" => 4,
//        "ID" => 3638,
    );
//    pre($arFilter);
    $arSelect = array(
        "ID",
        'DETAIL_TEXT'
    );
    $res = CIBlockElement::GetList(array('ID' => 'asc'), $arFilter, false, Array("nPageSize" => $limit, "iNumPage" => $page), $arSelect);
    $arResult['PAGE_COUNT'] = $res->NavPageCount;
    $arResult['PAGE_ITEM'] = $res->NavPageNomer;
    $arResult['PAGE_IS_NEXT'] = $arResult['PAGE_ITEM'] < $arResult['PAGE_COUNT'];

    $count = $limit * $arResult['PAGE_COUNT'];
    echo '<h3>Выполнено ' . round(($page - 1) / ceil($count / $limit) * 100, 5) . '% (' . ($page - 1) * $limit . '/' . $res->SelectedRowsCount() . ')</h3>';
    $el = new CIBlockElement;
    while ($arItem = $res->Fetch()) {
        $both = explode(',', $arItem['DETAIL_TEXT']);
        $items = $products = array();
        foreach ($both as $one) {
            list($oldId) = explode('|', $one);
            if (is_numeric($oldId)) {
                $items[] = $oldId;
            }
        }
        if (empty($items)) {
            continue;
        }
        $_SESSION['news'][] = $arItem['ID'];
        $arFilter = Array(
            "IBLOCK_ID" => 2,
            "PROPERTY_OLD_ID" => $items,
        );
        $arSelect = array(
            "ID",
        );
        $resProd = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
        while ($arProduct = $resProd->Fetch()) {
            $products[] = $arProduct['ID'];
        }
         CIBlockElement::SetPropertyValues($arItem['ID'], 4, $products, 'PRODUCT');
//        preExit($arItem, $items, $products);
    }

    pre($_SESSION['news']);
//    exit;
    echo '<h4>' . (time() - $time) . 'сек </h4>';
    if ($arResult['PAGE_IS_NEXT']) {
        $param = array(
            'page' => ++$page,
            'time' => time(),
        );
        echo '<META http-equiv="refresh" content="1; URL=?' . http_build_query($param) . '">';
        echo '<a href="?' . http_build_query($param) . '">next</>';
    } else {
        //    echo '<META http-equiv="refresh" content="1; URL=/import/product.clear.php">';
    }
}
