<?

use Bitrix\Main\Loader,
    Bitrix\Main\Application;

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

if (Loader::includeModule('import.catalog')) {
    $request = Application::getInstance()->getContext()->getRequest();
    $page = $request->get('page1') ?: 1;

    $time = time();
    $limit = 100;
    $CATALOG_ID = 2;

    $fileDebug = $_SERVER["DOCUMENT_ROOT"] . '/upload/tmp/log/catalog.duble.code.txt';
    $fileDectivate = $_SERVER["DOCUMENT_ROOT"] . '/upload/tmp/log/dectivate.txt';
    CheckDirPath($fileDebug);
    CheckDirPath($fileDectivate);

    $el = new CIBlockElement;

    $res = Application::getConnection()->query("SELECT
	SQL_CALC_FOUND_ROWS count(`iblock_element`.`CODE`) AS `cnt`,
	`iblock_element`.`CODE` AS `CODE`
    FROM `b_iblock_element` `iblock_element`
    WHERE `iblock_element`.`IBLOCK_ID` = 2
    AND (`iblock_element`.`CODE` IS NOT NULL AND LENGTH(`iblock_element`.`CODE`) > 0)
    AND UPPER(`iblock_element`.`ACTIVE`) like upper('Y')
    GROUP BY `iblock_element`.`CODE`
    HAVING count(`iblock_element`.`CODE`) > '1'
    LIMIT " . ($page - 1) * $limit . ", {$limit}");
    $count = Application::getConnection()->queryScalar('SELECT FOUND_ROWS() as TOTAL');
    $arResult['PAGE_COUNT'] = ceil($count / $limit);
    $arResult['PAGE_ITEM'] = $page;
    $arResult['PAGE_IS_NEXT'] = $arResult['PAGE_ITEM'] < $arResult['PAGE_COUNT'];

    echo '<h3>Выполнено ' . round(($page - 1) / ceil($count / $limit) * 100, 5) . '% (' . ($page - 1) * $limit . '/' . $count . ')</h3>';
    while ($arCount = $res->Fetch()) {
            $arFilter = Array(
                "IBLOCK_ID" => $CATALOG_ID,
                "CODE" => $arCount['CODE'],
                "ACTIVE" => 'Y',
            );
            $arSelect = array(
                'ID',
                'NAME',
                'CODE',
                'PREVIEW_PICTURE',
                'DETAIL_PICTURE',
                'PROPERTY_OLD_ID',
                'PROPERTY_ARTNUMBER',
                'PROPERTY_NEWPRODUCT',
                'PROPERTY_SALELEADER',
                'PROPERTY_SPECIALOFFER',
                'PROPERTY_MORE_PHOTO',
            );
            $products = array();
            $resProduct = CIBlockElement::GetList(array('ID' => 'ASC'), $arFilter, false, false, $arSelect);
            while ($arItem = $resProduct->Fetch()) {
                $products[$arItem['ID']] = $arItem;
            }

            $set = false;
            $imgCount = 0;
            foreach ($products as $key => $arItem) {
                if ($arItem['PROPERTY_MORE_PHOTO_VALUE'] and count($arItem['PROPERTY_MORE_PHOTO_VALUE']) > $imgCount) {
                    $set = $key;
                    $imgCount = count($arItem['PROPERTY_MORE_PHOTO_VALUE']);
                }
            }
            if ($set === false) {
                $set = key($products);
            }
            $arProduct = $products[$set];

//            preExit($arProduct, $products);

            $propFields = array();
            $imgCount = count($arProduct['PROPERTY_MORE_PHOTO_VALUE']);
            foreach ($products as $key => $arItem) {
                foreach (array('NEWPRODUCT', 'SALELEADER', 'SPECIALOFFER') as $value) {
                    $props = 'PROPERTY_' . $value . '_ENUM_ID';
                    if (empty($arProduct[$props]) and ! empty($arItem[$props])) {
                        $propFields[$value] = $arItem[$props];
                    }
                }
                foreach (array( 'OLD_ID') as $value) {
                    $props = 'PROPERTY_' . $value . '_VALUE';
                    if (empty($arProduct[$props]) and ! empty($arItem[$props])) {
                        $propFields[$value] = $arItem[$props];
                    }
                }

//                if (empty($arProduct['PROPERTY_MORE_PHOTO_VALUE']) and ! empty($arItem['PROPERTY_MORE_PHOTO_VALUE'])) {
                if (count($arItem['PROPERTY_MORE_PHOTO_VALUE']) > $imgCount) {
                    $propFields['MORE_PHOTO'] = array();
                    $imgCount = count($arItem['PROPERTY_MORE_PHOTO_VALUE']);
                    foreach ($arItem['PROPERTY_MORE_PHOTO_VALUE'] as $value) {
                        $propFields['MORE_PHOTO'][] = CFile::MakeFileArray(CFile::GetPath($value));
                    }
                }
//                }

                foreach (array('PREVIEW_PICTURE', 'DETAIL_PICTURE') as $value) {
                    if (empty($arProduct[$value]) and ! empty($arItem[$value])) {
                        $propFields[$value] = CFile::MakeFileArray(CFile::GetPath($arItem[$value]));
                    }
                }
            }
//            preExit($products, $arProduct, $propFields);

            if ($propFields) {
                foreach ($propFields as $key2 => $value2) {
                    pre($arProduct['ID'], $CATALOG_ID, $value2, $key2);
                    CIBlockElement::SetPropertyValues($arProduct['ID'], $CATALOG_ID, $value2, $key2);
                }
            }

            foreach ($products as $key => $arItem) {
                if ($arItem['ID'] != $arProduct['ID']) {
                    $arFields = array();
                    $arFields['ACTIVE'] = 'N';
                    pre($arItem['ID'], $arFields);
                    $el->Update($arItem['ID'], $arFields);
                }
            }
//            preExit($arProduct, $products);

            $result = PHP_EOL . 'найдены дубли: ' . implode(', ', array_keys($products));
            unset($products[$arProduct['ID']]);
            $result .= PHP_EOL . 'дееактивированы: ' . implode(', ', array_keys($products));
            preDebugFile($fileDebug, $result);
            preDebugFile($fileDectivate, implode(', ', array_keys($products)));

//            preExit($arProduct, $products);
//                if (empty($arItem['ACTIVE_FROM'])) {
//                    $arFields = array();
//                    $arFie$arFieldslds['ACTIVE_FROM'] = '01.05.2017 00:00:00';
//                    pre($arFields);
//                    $el->Update($arItem['ID'], $arFields);
//                }
    }
    echo '<h4>' . (time() - $time) . 'сек </h4>';
//    exit;
    if ($arResult['PAGE_IS_NEXT']) {
        $param = array(
            'page' => ++$page,
            'time' => time(),
        );
        echo '<META http-equiv="refresh" content="1; URL=?' . http_build_query($param) . '">';
        echo '<a href="?' . http_build_query($param) . '">next</>';
    } else {
        //    echo '<META http-equiv="refresh" content="1; URL=/import/product.clear.php">';
    }
}
