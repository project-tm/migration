<?php

use Bitrix\Main\Application;

/**
 * Class Migration1533130272
 *
 * @author       : bitrix
 * @documentation: http://cjp2600.github.io/bim-core/
 */
class Migration1533130272 implements Bim\Revision
{

    private static $author = "Popov Alexej";
    private static $description = "add index optimize";

    const INDEX = <<<INDEX
CREATE INDEX ix_perf_megafon_b_agent_1 ON b_agent (MODULE_ID);
CREATE INDEX ix_perf_megafon_b_event_type_1 ON b_event_type (LID);
CREATE INDEX ix_perf_megafon_b_group_1 ON b_group (ACTIVE);
CREATE INDEX ix_perf_megafon_b_group_2 ON b_group (ACTIVE,ANONYMOUS);
CREATE INDEX ix_perf_megafon_b_group_3 ON b_group (ACTIVE,ID);
CREATE INDEX ix_perf_megafon_b_hlblock_entity_1 ON b_hlblock_entity (NAME);
CREATE INDEX ix_perf_megafon_b_hlblock_entity_2 ON b_hlblock_entity (TABLE_NAME);
CREATE INDEX ix_perf_megafon_b_iblock_1 ON b_iblock (LID);
CREATE INDEX ix_perf_megafon_b_iblock_2 ON b_iblock (ACTIVE);
CREATE INDEX ix_perf_megafon_b_iblock_3 ON b_iblock (CODE);
CREATE INDEX ix_perf_megafon_b_iblock_property_1 ON b_iblock_property (ACTIVE,IBLOCK_ID);
CREATE INDEX ix_perf_megafon_b_iblock_property_2 ON b_iblock_property (ACTIVE,PROPERTY_TYPE);
CREATE INDEX ix_perf_megafon_b_iblock_property_3 ON b_iblock_property (ACTIVE,USER_TYPE);
CREATE INDEX ix_perf_megafon_b_iblock_section_1 ON b_iblock_section (ACTIVE,IBLOCK_ID);
CREATE INDEX ix_perf_megafon_b_iblock_section_1 ON b_iblock_section (GLOBAL_ACTIVE);
CREATE INDEX ix_perf_megafon_b_iblock_site_1 ON b_iblock_site (SITE_ID);
CREATE INDEX ix_perf_megafon_b_iblock_type_lang_1 ON b_iblock_type_lang (IBLOCK_TYPE_ID);
CREATE INDEX ix_perf_megafon_b_lang_1 ON b_lang (ACTIVE);
CREATE INDEX ix_perf_megafon_b_lang_2 ON b_lang (ACTIVE,CULTURE_ID);
CREATE INDEX ix_perf_megafon_b_lang_3 ON b_lang (CULTURE_ID);
CREATE INDEX ix_perf_megafon_b_language_1 ON b_language (CULTURE_ID);
CREATE INDEX ix_perf_megafon_b_language_2 ON b_language (ACTIVE);
CREATE INDEX ix_perf_megafon_b_language_3 ON b_language (ACTIVE,CULTURE_ID);
CREATE INDEX ix_perf_megafon_b_language_4 ON b_language (ACTIVE,LID);
CREATE INDEX ix_perf_megafon_b_list_rubric_1 ON b_list_rubric (ACTIVE,LID,VISIBLE);
CREATE INDEX ix_perf_megafon_b_list_rubric_2 ON b_list_rubric (ACTIVE);
CREATE INDEX ix_perf_megafon_b_operation_1 ON b_operation (BINDING,MODULE_ID);
CREATE INDEX ix_perf_megafon_b_user_1 ON b_user (ACTIVE);
CREATE INDEX ix_perf_megafon_b_user_field_lang_1 ON b_user_field_lang (LANGUAGE_ID);
CREATE INDEX ix_perf_megafon_b_favorite_1 ON b_favorite (`USER_ID`, `LANGUAGE_ID`, `URL`(250));
CREATE INDEX ix_perf_megafon_b_megafon_version_control ON b_megafon_version_control (`ELEMENT_ID`, `USER_ID`, `TIME`);
INDEX;

    private static function getIndex() {
        $index = explode(PHP_EOL, self::INDEX);
        $AL_VARIANTS_ID = Utils::getIBlockIdByCode('al_variants', 'algorithms');
        $index[] = "CREATE INDEX ix_perf_megafon_b_iblock_element_prop_m{$AL_VARIANTS_ID}_1 ON b_iblock_element_prop_m{$AL_VARIANTS_ID} (IBLOCK_ELEMENT_ID,IBLOCK_PROPERTY_ID,VALUE_NUM);";
        return $index;
    }

    private static function query($query)
    {
        try {
            Application::getConnection()->query($query);
        } catch (Exception $ex) {
        }
    }

    /**
     * up
     *
     * @success : return void or true;
     * @error   : return false, or Exception
     */
    public static function up()
    {
        foreach (self::getIndex() as $index) {
            self::query($index);
        }
    }

    /**
     * down
     *
     * @success : return void or true;
     * @error   : return false, or Exception
     */
    public static function down()
    {
        foreach (self::getIndex() as $index) {
            $data = explode(' ', $index);
            self::query("ALTER TABLE `{$data[4]}` DROP INDEX `{$data[2]}`;");
        }
    }

    /**
     * getDescription
     *
     * @return string
     */
    public static function getDescription()
    {
        return self::$description;
    }

    /**
     * getAuthor
     *
     * @return string
     */
    public static function getAuthor()
    {
        return self::$author;
    }

}
