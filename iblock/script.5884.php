<?php

// если скрипт запущен не из под консоли, запрещаем доступ
$sapi_type = php_sapi_name();

if (substr($sapi_type, 0, 3) != 'cli') {
    echo 'access denied';
    die();
}

$_SERVER['DOCUMENT_ROOT'] = realpath(__DIR__ . '/../..');
$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];

define('NO_KEEP_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS', true);
define('BX_CRONTAB', true);
define('BX_NO_ACCELERATOR_RESET', true);

require_once($DOCUMENT_ROOT . '/bitrix/modules/main/include/prolog_before.php');

$error = array();
if (\Bitrix\Main\Loader::includeModule('iblock')) {

    $properties = CIBlockProperty::GetList(Array(), Array("CODE" => "CARD_LOYALTY", "IBLOCK_ID" => \Tools\Constants::$IBLOCK_APPLICATIONS));
    if (!$prop_fields = $properties->GetNext()) {
        $arFields = Array(
            "NAME" => "№ карты лояльности МЕТРО",
            "ACTIVE" => "Y",
            "SORT" => "500",
            "CODE" => "CARD_LOYALTY",
            "PROPERTY_TYPE" => "S",
            "IBLOCK_ID" => \Tools\Constants::$IBLOCK_APPLICATIONS
        );
        $ibp = new CIBlockProperty;
        if ($PropID = $ibp->Add($arFields)) {
            $errors[] = 'Свойство добавлено CARD_LOYALTY';
        } else {
            $errors[] = 'Ошибка добавления свойства CARD_LOYALTY: ' . $ibp->LAST_ERROR;
        }
    } else {
        $errors[] = 'Свойство уже есть CARD_LOYALTY';
    }

    $properties = CIBlockProperty::GetList(Array(), Array("CODE" => "IS_CARD_LOYALTY", "IBLOCK_ID" => \Tools\Constants::$IBLOCK_PARTNERS));
    if (!$prop_fields = $properties->GetNext()) {
        $arFields = Array(
            "NAME" => "Регистрация: Показывать \"№ карты лояльности МЕТРО\"",
            "ACTIVE" => "Y",
            "SORT" => "500",
            "CODE" => "IS_CARD_LOYALTY",
            "PROPERTY_TYPE" => "S",
            "USER_TYPE" => "Checkbox",
            "IBLOCK_ID" => \Tools\Constants::$IBLOCK_PARTNERS
        );
        $ibp = new CIBlockProperty;
        if ($PropID = $ibp->Add($arFields)) {
            $errors[] = 'Свойство добавлено IS_CARD_LOYALTY';
        } else {
            $errors[] = 'Ошибка добавления свойства IS_CARD_LOYALTY: ' . $ibp->LAST_ERROR;
        }
    } else {
        $errors[] = 'Свойство уже есть IS_CARD_LOYALTY';
    }
}
echo implode(PHP_EOL, $errors) . PHP_EOL;
