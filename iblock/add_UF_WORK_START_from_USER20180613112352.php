<?php

namespace Sprint\Migration;


class add_UF_WORK_START_from_USER20180613112352 extends \Adv\Bitrixtools\Migration\SprintMigrationBase
{

    protected $description = "Создаем пользовательское поле UF_WORK_START у объекта USER ";

    public function up()
    {
        $helper = new HelperManager();

        /*
         * UserType
         */

        $helper->UserTypeEntity()->addUserTypeEntitiesIfNotExists(
            'USER', [
                [
                    'ENTITY_ID'         => 'USER',
                    'FIELD_NAME'        => 'UF_WORK_START',
                    'USER_TYPE_ID'      => 'string',
                    'XML_ID'            => 'UF_WORK_START',
                    'SORT'              => '100',
                    'MULTIPLE'          => null,
                    'MANDATORY'         => null,
                    'SHOW_FILTER'       => 'N',
                    'SHOW_IN_LIST'      => null,
                    'EDIT_IN_LIST'      => null,
                    'IS_SEARCHABLE'     => null,
                    'SETTINGS'          =>
                        [
                            'DEFAULT_VALUE' => '',
                            'SIZE'          => '20',
                            'ROWS'          => '1',
                            'MIN_LENGTH'    => '0',
                            'MAX_LENGTH'    => '0',
                            'REGEXP'        => '',
                        ],
                    'EDIT_FORM_LABEL'   =>
                        [
                            'ru' => 'Время прима на работу',
                            'en' => 'Время прима на работу',
                        ],
                    'LIST_COLUMN_LABEL' =>
                        [
                            'ru' => '',
                            'en' => '',
                        ],
                    'LIST_FILTER_LABEL' =>
                        [
                            'ru' => '',
                            'en' => '',
                        ],
                    'ERROR_MESSAGE'     =>
                        [
                            'ru' => '',
                            'en' => '',
                        ],
                    'HELP_MESSAGE'      =>
                        [
                            'ru' => '',
                            'en' => '',
                        ],
                ],
            ]
        );
    }

    public function down()
    {
        $helper = new HelperManager();

        $helper->UserTypeEntity()->deleteUserTypeEntitiesIfExists('USER', ['UF_WORK_START']);
    }

}
