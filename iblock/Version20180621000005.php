<?php

namespace Sprint\Migration;

class Version20150520000005 extends Version {

    const ANSWER_OSA_STATUS = 'AnswerOsaStatus';

    protected $description = "Добавляем Hl инфоблок";

    public function up(){

        $helper = new HelperManager();

        $hlblockId = $helper->Hlblock()->addHlblockIfNotExists(array(
            'NAME' => self::ANSWER_OSA_STATUS,
            'TABLE_NAME' => 'hl_answer_osa_status',
        ));

        if($hlblockId) {
            $helper->UserTypeEntity()->addUserTypeEntitiesIfNotExists(
                'HLBLOCK_' . $hlblockId,
                [
                    ['FIELD_NAME' => 'UF_NAME', "USER_TYPE_ID" => "string"],
                    ['FIELD_NAME' => 'UF_PRICE', "USER_TYPE_ID" => "integer"],
                    ['FIELD_NAME' => 'UF_WEIGHT', "USER_TYPE_ID" => "double"],
                    ['FIELD_NAME' => 'UF_CREATED_AT', "USER_TYPE_ID" => "datetime"],
                    ['FIELD_NAME' => 'UF_UPDATED_AT', "USER_TYPE_ID" => "datetime"],
                ]
            );
        }
        $this->viewResut($status,'Инфоблок удален', 'Ошибка удаления инфоблока');

        $iblockId1 = $helper->Iblock()->findIblock('');
        if($helper->Iblock()) {
            $helper->Iblock()->addPropertyIfNotExists($iblockId1, array(
                'NAME' => 'Ссылка',
                'CODE' => 'LINK',
            ));
        } else {
            $this->outError('Инфоблок вопрос не существует');
        }
    }

    public function down(){
        $helper = new HelperManager();
        if ($hlblockId = $helper->Hlblock()->getHlblockId(self::ANSWER_OSA_STATUS)) {
            $helper->UserTypeEntity()->deleteUserTypeEntitiesIfExists(
                'HLBLOCK_' . $hlblockId,
                [
                    'UF_NAME',
                    'UF_PRICE',
                    'UF_WEIGHT',
                    'UF_CREATED_AT',
                    'UF_UPDATED_AT',
                ]
            );
            $status = $helper->Hlblock()->deleteHlblock($hlblockId);
            $this->viewResut($status,'Инфоблок удален', 'Ошибка удаления инфоблока');
        }
    }

    private function viewResut($status, $uspech, $ochibka) {
        if ($status){
            $this->outSuccess($uspech);
        } else {
            $this->outError($ochibka);
        }
    }

}
