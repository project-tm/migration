<?php

use Bitrix\Main\Loader;

/**
 * Class Migration1533130272
 *
 * @author       : bitrix
 * @documentation: http://cjp2600.github.io/bim-core/
 */
class Migration1533130272 implements Bim\Revision
{

    private static $author = "Popov Alexej";
    private static $description = "add index optimize";

    /**
     * up
     *
     * @success : return void or true;
     * @error   : return false, or Exception
     */
    public static function up()
    {
        $id = 'megafon.tagcache';
        if (!Loader::includeModule($id) and $Module = CModule::CreateModuleObject($id)) {
            if ($Module->DoInstall() === false) {
                global $APPLICATION, $errorMessage;
                $errorMessage = GetMessage("MOD_INSTALL_ERROR", ["#CODE#" => $id]);
                if ($e = $APPLICATION->GetException()) {
                    throw new \Exception($e->GetString());;
                }
            }
        }
    }

    /**
     * down
     *
     * @success : return void or true;
     * @error   : return false, or Exception
     */
    public static function down()
    {
        $id = 'megafon.tagcache';
        if (Loader::includeModule($id) and $Module = CModule::CreateModuleObject($id)) {
            if ($Module->DoUninstall() === false) {
                global $APPLICATION, $errorMessage;
                $errorMessage = GetMessage("MOD_INSTALL_ERROR", ["#CODE#" => $id]);
                if ($e = $APPLICATION->GetException()) {
                    throw new \Exception($e->GetString());;
                }
            }
        }
    }

    /**
     * getDescription
     *
     * @return string
     */
    public static function getDescription()
    {
        return self::$description;
    }

    /**
     * getAuthor
     *
     * @return string
     */
    public static function getAuthor()
    {
        return self::$author;
    }

}
